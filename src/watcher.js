(function() {
'use strict';

var chokidar = require('chokidar');  

var watch = function watch(socket,location,options) {
    
  var watchOptions = {
    ignored: undefined,
    persistent: true,
    depth: 1,
    cwd: location,
    usePolling: true
  };

  var watcher = chokidar.watch(location,watchOptions);

    watcher.on('all', function(event, path, stat) {
      socket.emit(event, path, stat);
    });

    watcher.on('ready', function() {
      socket.emit('ready');
    });

    watcher.on('error', function(error) {
      socket.emit('fsError',error);
    });

  socket.on('forceDisconnect', function () {
    socket.disconnect();
  });

  socket.on('disconnect', function () {
    watcher.close();
  });
};

module.exports = exports = watch;

})();